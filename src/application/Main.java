/**
 * This source code is Copyright 2015 by Supnaat Pokturng.
 */
package application;

import java.util.Timer;
import java.util.TimerTask;

import clock.CheapClock;
import clock.ClockGUI;
import clock.ClockTask;
/**
 * Main class for execute the program.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class Main {
	/**
	 * Execute the program.
	 * @param args is an arguments
	 */
	public static void main( String [] args ) {
		/* Create clock */
		CheapClock clock = new CheapClock();
		/* Create clock interface */
		ClockGUI clockGUI = new ClockGUI();
		clock.addObserver(clockGUI);
		TimerTask clocktask = new ClockTask( clock );
		Timer timer = new Timer();
		/* Update time every 1 second */
		timer.scheduleAtFixedRate( clocktask, 1000 , 1000 );
		
	}
}
