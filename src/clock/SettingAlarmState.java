/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * Setting alarm time state that include 
 * set hour, minute, second inside. 
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
abstract class SettingAlarmState implements State{
	public final static State SET_HOUR = new SetHourState();
	public final static State SET_MINUTE = new SetMinuteState();
	public final static State SET_SECOND = new SetSecondState();
	
	/**
	 * Set the show time to alarm time.
	 * @param clock is the clock
	 */
	public void showTime(CheapClock clock){
		clock.currentTime = clock.alarmTime;
	};
}
