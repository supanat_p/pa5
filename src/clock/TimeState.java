/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * State of timing.
 * @author Supanat Polturng
 * @version 2015.04.12
 */
public class TimeState implements State {

	/**
	 * Change to setting hour state of the clock.
	 * @param clock is the clock
	 */
	public void pressSet(CheapClock clock) {
		clock.setIsTiming(false);
		clock.setState(clock.getSetHourState());
		clock.isSetHour = true;
	}
	
	/**
	 * @param clock is the clock.
	 */
	public void pressPlus(CheapClock clock) {	
	}

	/**
	 * Setting alarm mode on or off.
	 * @param clock is the clock
	 */
	public void pressMinus(CheapClock clock) {
		if(clock.isAlarm)
			clock.isAlarm = false;
		else
			clock.isAlarm = true;
	}

	/**
	 * Show the current time of the clock.
	 * @param clock is the clock
	 */
	public void showTime(CheapClock clock) {
		clock.currentTime = clock.time;
	}

}
