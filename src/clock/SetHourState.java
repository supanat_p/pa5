/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * State of setting hour of alarm time.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class SetHourState extends SettingAlarmState {

	/**
	 * Change to setting minute state.
	 * @param clock is the clock
	 */
	public void pressSet(CheapClock clock) {
		clock.updateTime();
		clock.setState(clock.getSetMinuteState());
		clock.isSetHour = false;
		clock.isSetMinute = true;
		
	}

	/**
	 * Increase hour of alarm time by 1.
	 * @param clock is the clock
	 */
	public void pressPlus(CheapClock clock) {
		int hour = clock.getHour() + 1;
		clock.currentTime.setHours(Math.floorMod( hour , 24 ));
		clock.updateTime();
	}

	/**
	 * Decrease hour of alarm time by 1.
	 * @param clock is the clock
	 */
	public void pressMinus(CheapClock clock) {
		int hour = clock.getHour() - 1;
		clock.currentTime.setHours(Math.floorMod( hour , 24 ));
		clock.updateTime();
	}

}
