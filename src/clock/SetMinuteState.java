/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * State of setting minute of alarm time of the clock.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class SetMinuteState extends SettingAlarmState {

	/**
	 * Change to setting second state of the clock.
	 * @param clock is the clock
	 */
	public void pressSet(CheapClock clock) {
		clock.setState(clock.getSetSecondState());
		clock.updateTime();
		clock.isSetMinute = false;
		clock.isSetSecond = true;
	}

	/**
	 * Increase minute of alarm time of the clock by 1.
	 * @param clock is the clock
	 */
	public void pressPlus(CheapClock clock) {
		int minute = clock.getMinute() + 1;
		clock.currentTime.setMinutes(Math.floorMod( minute , 60 ) );
		clock.updateTime();
		
	}

	/**
	 * Decrease minute of alarm time of the clock by 1.
	 * @param clock is the clock
	 */
	public void pressMinus(CheapClock clock) {
		int minute = clock.getMinute() - 1;
		clock.currentTime.setMinutes(Math.floorMod( minute , 60 ) );
		clock.updateTime();
		
	}
	
}
