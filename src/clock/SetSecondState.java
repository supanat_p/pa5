/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * State of setting second of alarm time of the clock.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class SetSecondState extends SettingAlarmState {

	/**
	 * Change state to timing state of the clock.
	 * @param clock is the clock
	 */
	public void pressSet(CheapClock clock) {
		clock.setState(clock.getTimeState());
		clock.updateTime();
		clock.isSetSecond = false;
	}

	/**
	 * Increase second of alarm time of the clock by 1.
	 * @param clock is the clock
	 */
	public void pressPlus(CheapClock clock) {
		int second = clock.getSecond() + 1;
		clock.currentTime.setSeconds(Math.floorMod( second , 60 ) );
		clock.updateTime();
	}

	/**
	 * Decrease second of alarm time of the clock by 1.
	 * @param clock is the clock
	 */
	public void pressMinus(CheapClock clock) {
		int second = clock.getSecond() - 1;
		clock.currentTime.setSeconds(Math.floorMod( second , 60 ) );
		clock.updateTime();
	}

}
