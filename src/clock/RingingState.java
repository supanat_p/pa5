/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * Ringing state of the clock.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class RingingState implements State {
	
	
	/**
	 * Stop ringing and change to timing state.
	 * @param clock is a clock
	 */
	public void pressSet(CheapClock clock) {
		clock.getClip().stop();
		clock.setState(clock.getTimeState());
	}

	/**
	 * Stop ringing and change to timing state.
	 * @param clock is a clock
	 */
	public void pressPlus(CheapClock clock) {
		clock.getClip().stop();
		clock.setState(clock.getTimeState());
	}

	/**
	 * Stop ringing and change to timing state.
	 * @param clock is a clock
	 */
	public void pressMinus(CheapClock clock) {
		clock.getClip().stop();
		clock.setState(clock.getTimeState());
	}

	/**
	 * Show current time of the clock.
	 * @param clock is the clock
	 */
	public void showTime(CheapClock clock) {
		clock.currentTime = clock.time;
	}

}
