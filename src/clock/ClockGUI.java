/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Graphic user interface of Clock.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class ClockGUI extends JFrame implements Observer {
	
	/* Declare and initialize */
	private JLabel lb_hour , lb_minute , lb_second , lb_colon1 , lb_colon2 , lb_isAlarm;
	private JButton btn_set , btn_plus , btn_minus;
	private CheapClock clock;
	private String[] onOff = {"ON" , "OFF"};
	
	/**
	 * Constructor of interface of the clock.
	 */
	public ClockGUI() {
		initComponents();
	}
	
	/**
	 * Initialize the components of interface of the clock.
	 */
	public void initComponents() {
		
		/* Set title of the window. */
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
		setTitle( "Cheap Digital Clock" );
		
		lb_hour = new JLabel("0");
		lb_hour.setForeground(Color.green);
		lb_minute = new JLabel("0");
		lb_minute.setForeground(Color.green);
		lb_second = new JLabel("0");
		lb_second.setForeground(Color.green);
		lb_colon1 = new JLabel(":");
		lb_colon1.setForeground(Color.green);
		lb_colon2 = new JLabel(":");
		lb_colon2.setForeground(Color.green);
		lb_isAlarm = new JLabel("Alarm : OFF");
		lb_isAlarm.setForeground(Color.green);
		btn_set = new JButton("SET");
		btn_set.setBackground(Color.lightGray);
		btn_plus = new JButton("+");
		btn_plus.setBackground(Color.lightGray);
		btn_minus = new JButton("-");
		btn_minus.setBackground(Color.lightGray);
		
		/**
		 * Add action to this button.
		 */
		btn_set.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				/* Call pressSet method in current state. */
				clock.getState().pressSet(clock);
				
			}
		});
		
		/**
		 * Add action to this button.
		 */
		btn_plus.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				/* Call pressPlus method in current state. */
				clock.getState().pressPlus(clock);
				
			}
		});
		
		/**
		 * Add action to this button.
		 */
		btn_plus.addMouseListener( new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				/* Show alarm time of this clock. */
				clock.currentTime = clock.alarmTime;
				int hour = clock.currentTime.getHours();
				int minute = clock.currentTime.getMinutes();
				int second = clock.currentTime.getSeconds();
				updateTime( hour , minute , second );
			}
			
		});
		
		/**
		 * Add action of this button.
		 */
		btn_minus.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				/* Call pressMinus method of current state. */
				clock.getState().pressMinus(clock);
				
			}
		});
		
		/* Add each content to container. */
		Container container = super.getContentPane();
		container.setLayout( new BoxLayout( container , BoxLayout.Y_AXIS));
		Panel paneTop = new Panel();
		paneTop.setLayout( new FlowLayout() );
		Panel paneBot = new Panel();
		paneBot.setLayout( new FlowLayout() );
		paneTop.add(lb_hour);
		paneTop.add(lb_colon1);
		paneTop.add(lb_minute);
		paneTop.add(lb_colon2);
		paneTop.add(lb_second);
		paneBot.add(btn_set);
		paneBot.add(btn_plus);
		paneBot.add(btn_minus);
		container.add(paneTop);
		container.add(lb_isAlarm);
		container.add(paneBot);
		container.setBackground(Color.black);
		
		pack();
		super.setVisible(true);
	}

	/**
	 * Update in interface of the clock.
	 * @param observe is an observable that observe this
	 * @param obj is and object
	 */
	public void update(Observable observe, Object obj) {
		Date time=null;
		if(observe.getClass() == CheapClock.class )
			this.clock = (CheapClock)observe;
		if(obj.getClass() == Date.class)
			time = (Date)obj;
		int hour = time.getHours();
		int minute = time.getMinutes();
		int second = time.getSeconds();
		/* Update time of the clock. */
		updateTime( hour , minute , second );
		/* Set label to show that alarm is on or not */
		if(this.clock.isAlarm)
			lb_isAlarm.setText("Alarm : "+onOff[0]);
		else
			lb_isAlarm.setText("Alarm : "+onOff[1]);
		
		/* Change color to red when setting alarm time. */
		if(clock.isSetHour) {
			lb_hour.setForeground(Color.red);
			lb_minute.setForeground(Color.green);
			lb_second.setForeground(Color.green);
		}
		else if(clock.isSetMinute) {
			lb_hour.setForeground(Color.green);
			lb_minute.setForeground(Color.red);
			lb_second.setForeground(Color.green);
		}
		else if(clock.isSetSecond) {
			lb_hour.setForeground(Color.green);
			lb_minute.setForeground(Color.green);
			lb_second.setForeground(Color.red);
		}
		else {
			lb_hour.setForeground(Color.green);
			lb_minute.setForeground(Color.green);
			lb_second.setForeground(Color.green);
		}
	}
	
	/**
	 * Set a time to the label.
	 * @param hour is an hour that want to set.
	 * @param minute is a minute that want to set.
	 * @param second is a second that want to set.
	 */
	public void updateTime( int hour , int minute , int second ) {
		lb_hour.setText(hour+"");
		lb_minute.setText(minute+"");
		lb_second.setText(second+"");
	}
	
}
