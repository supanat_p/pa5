/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

/**
 * Interface of every states.
 * @author Supanat Polturng
 * @version 2015.04.12
 */
public interface State {
	
	/**
	 * Doing something when press set button.
	 * @param clock is the clock
	 */
	public void pressSet(CheapClock clock);
	
	/**
	 * Doing something when press plus button.
	 * @param clock is the clock
	 */
	public void pressPlus(CheapClock clock);
	
	/**
	 * Doing something when press minus button.
	 * @param clock is the clock
	 */
	public void pressMinus(CheapClock clock);
	
	/**
	 * Show the time on the label.
	 * @param clock is the clock
	 */
	public void showTime(CheapClock clock);
}
