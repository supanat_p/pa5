/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

import java.util.TimerTask;

/**
 * Task of the clock.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class ClockTask extends TimerTask{
	
	private CheapClock clock;
	
	/**
	 * Constructor of the task.
	 * @param clock is a clock that use this task
	 */
	public ClockTask(CheapClock clock) {
		this.clock = clock;
	}
	
	/**
	 * Run the task.
	 */
	public void run( ) {
		clock.updateTime( );
	}

}
