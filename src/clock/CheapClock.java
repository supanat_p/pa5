/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package clock;

import java.io.IOException;
import java.util.Date;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Cheap clock class.
 * Can set alarm time and alert.
 * @author Supanat Pokturng
 * @version 2015.04.12
 */
public class CheapClock extends Observable {
	/* Declare and initialize */
	State state;
	private State timeState = new TimeState();
	private State ringingState = new RingingState();
	private State setHourState = new SetHourState();
	private State setMinuteState = new SetMinuteState();
	private State setSecondState = new SetSecondState();
	protected Date alarmTime;
	protected Date time;
	protected Date currentTime;
	private boolean isTiming;
	private Clip clip;
	private AudioInputStream audioIn;
	protected boolean isAlarm , isSetHour , isSetMinute , isSetSecond;
	
	/**
	 * Constructor of the clock.
	 */
	public CheapClock() {
		state = timeState;
		isTiming = true;
		time = new Date();
		alarmTime = new Date();
		alarmTime.setTime(28800000);
		isAlarm = false;
		isSetHour = false;
		isSetMinute = false;
		isSetSecond = false;
	}
	
	/**
	 * Set a new state.
	 * @param newState is a new state that want to change
	 */
	public void setState(State newState) {
		this.state = newState;
	}
	
	/**
	 * Get current state of the clock.
	 * @return State of current state
	 */
	public State getState() {
		return this.state;
	}
	
	/**
	 * Get Timing state.
	 * @return State of timing state
	 */
	public State getTimeState() {
		return this.timeState;
	}
	
	/**
	 * Get Ringing state.
	 * @return State of ringing state
	 */
	public State getRingingState() {
		return this.ringingState;
	}
	
	/**
	 * Get SetHour state.
	 * @return State of setting hour state
	 */
	public State getSetHourState() {
		return this.setHourState;
	}
	
	/**
	 * Get SetMinute state.
	 * @return State of setting minute state
	 */
	public State getSetMinuteState() {
		return this.setMinuteState;
	}
	
	/**
	 * Get SetSecond state.
	 * @return State of setting second state
	 */
	public State getSetSecondState() {
		return this.setSecondState;
	}
	
	/**
	 * Get current hour of the clock.
	 * @return integer of hour
	 */
	public int getHour() {
		return currentTime.getHours();
	}
	
	/**
	 * Get current minute of the clock.
	 * @return integer of minute
	 */
	public int getMinute() {
		return currentTime.getMinutes();
	}
	
	/**
	 * Get current second of the clock.
	 * @return integer of second
	 */
	public int getSecond() {
		return currentTime.getSeconds();
	}
	
	/**
	 * Set a new status of timing of the clock. 
	 * @param isTiming is a new status of timing 
	 */
	public void setIsTiming(boolean isTiming) {
		this.isTiming = isTiming;
	}
	
	/**
	 * Update a new time of clock.
	 */
	public void updateTime() {
		time.setTime(System.currentTimeMillis());
		/* Check this time is alarm time or not. */
		if(checkAlarm() && isAlarm) {
			try {
		         /* Open an audio input stream. */
		         audioIn = AudioSystem.getAudioInputStream(getClass()
		        		 .getResource("/sounds/alarm.wav"));
		         /* Get a sound clip resource. */
		         clip = AudioSystem.getClip();
		         /* Open audio clip and load samples from the audio input stream. */
		         clip.open(audioIn);
		         clip.start();
		         /* Play sound until stop */
		         clip.loop(clip.LOOP_CONTINUOUSLY);
		      } catch (UnsupportedAudioFileException e) {
		         e.printStackTrace();
		      } catch (IOException e) {
		         e.printStackTrace();
		      } catch (LineUnavailableException e) {
		         e.printStackTrace();
		      }
			setIsTiming(false);
			this.state = ringingState;
		}
		state.showTime(this);
		setChanged();
		notifyObservers(currentTime);
	}
	
	/**
	 * Get a clip.
	 * @return a clip
	 */
	public Clip getClip() {
		return this.clip;
	}
	
	/**
	 * Check this time is alarm time or not.
	 * @return true if this time is equals alarm time
	 */
	public boolean checkAlarm() {
		return ((time.getHours() == alarmTime.getHours()) &&
				(time.getMinutes() == alarmTime.getMinutes() ) &&
				(time.getSeconds() == alarmTime.getSeconds() ) );
	}
}
